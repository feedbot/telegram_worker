REDIS_CONFIG = {
    'host': 'localhost',
    'port': 6379,
    'db': 11,
}

BLOOM_CONFIG = {
    'host': 'localhost',
    'port': 6379,
    'db': 11,
    'redis_key': 'TG_BLOOM',
    'capacity': 100000,
    'error_rate': 0.00000001,
    'count': 2
}

MONGO_CONFIG = {
    'db': 'feed_bot',
    'host': 'mongo-primary.service.dc1.consul',
    'port': 27017,
    'connect': False,
}


API_ID = '314676'
API_HASH = 'eef5147628a4d7a93721a2b8df069400'

REDIS_CHANNEL_PREFIX = 'channels'

CHANNEL_JOIN_SLEEP = 600
BAN_CHECK_SLEEP = 3600

CHANNELS_PER_PROFILE = 450


FEED_BOT_NAME = 'feedb0t_bot'
