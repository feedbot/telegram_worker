import sys
import asyncio
import logging
# from .async_tasks import run_client, join_chat
from socionet.telegram_scraper.async_tasks import run_client, join_chat
from socionet.telegram_scraper.sync_tasks import request_code, authorize_user
# from telethon import sync

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)


if __name__ == '__main__':
    sessions = ['+380639615614']
    # sessions = ['+380631758756']
    # sessions = ['+380631758764', '+380631758766']
    # sessions = ['+380631758764']
    # sessions = ['+380631758724']
    # sessions = ['+380631758714']
    # sessions = ['+380631541955']
    # sessions = ['+380639615614']

    # with open('/home/pata/PycharmProjects/telegram_scraper/channels.txt') as f:
    #     channels = [x.strip() for x in f.readlines()]

    tasks = [join_chat(sessions[0], 'Ds93sRRxk3TncfNdbqk8EA', private=True, update_mongo=False), run_client(sessions[0])]
    print('(Press Ctrl+C to stop this)')

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(tasks))

    # print(authorize_user('+380639615614', '43964'))

